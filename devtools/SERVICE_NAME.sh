#!/bin/bash

# TODO: Parametrize log symbol
info_logger() {
  echo "[.] $1"
}

confirm_logger() {
  echo "[+]" $1
}

error_logger() {
  echo "[!] $1"
}

# Local (Development) Ops Infrastructure
# =======================================

# Parse Arguments
ACTION=$1

# Parse Options. Use `export` for
# `yq`'s `strenv` function
export SECRETS_CONTAINER_NAME=unset
export SECRETS_HOST=unset
export SECRETS_KEY=unset
export SECRETS_TOKEN=unset
export DATABASE_CONTAINER_NAME=unset
export DATABASE_HOST=unset
export DATABASE_USER=unset

usage()
{
  echo "Usage: {{SERVICE_NAME}} [ create | delete ] " \
  "[ -s | --secrets-container-name ] " \
  "[ -h | --secrets-host ] " \
  "[ -k | --secrets-key ] " \
  "[ -t | --secrets-token ] " \
  "[ -d | --database-container-name ] " \
  "[ -b | --database-host ] " \
  "[ -u | --database-user ] " \
  "[ -p | --project-root ]"
  exit 2
}

PARSED_ARGUMENTS=$(getopt -a -n {{SERVICE_NAME}} \
-o s:h:k:t:d:b:u:p: \
--long secrets-container-name:,\
secrets-host:,\
secrets-key:,\
secrets-token:,\
database-container-name:,\
database-host:,\
database-user:,\
project-root: -- "$@")
VALID_ARGUMENTS=$?
if [ "$VALID_ARGUMENTS" != "0" ]; then
  usage
fi

eval set -- "$PARSED_ARGUMENTS"
while :
do
  case "$1" in
    -s | --secrets-container-name)  SECRETS_CONTAINER_NAME="$2"  ; shift 2 ;;
    -h | --secrets-host)            SECRETS_HOST="$2"            ; shift 2 ;;
    -k | --secrets-key)             SECRETS_KEY="$2"             ; shift 2 ;;
    -t | --secrets-token)           SECRETS_TOKEN="$2"           ; shift 2 ;;
    -d | --database-container-name) DATABASE_CONTAINER_NAME="$2" ; shift 2 ;;
    -b | --database-host)           DATABASE_HOST="$2"           ; shift 2 ;;
    -u | --database-user)           DATABASE_USER="$2"           ; shift 2 ;;
    -p | --project-root)            PROJECT_ROOT="$2"            ; shift 2 ;;
    --) shift; break ;;
    *) echo "Unexpected option: $1"
      usage ;;
  esac
done

# Helper Functions

function get_service_url() {
  if [[ $LOCAL_DEV ]]
  then
    export SERVICE_URL=$(minikube service {{SERVICE_NAME}}-service --url)
  else
    export RAW_INGRESS_IP=$(kubectl get -o json service | jq '.items[1].status.loadBalancer.ingress[0].ip')
    export INGRESS_IP=$(echo $RAW_INGRESS_IP | sed "s/\"//g")
    export INGRESS_PORT=$(kubectl get -o json service | jq '.items[1].spec.ports[0].port')
    export SERVICE_URL="http://$INGRESS_IP:$INGRESS_PORT"
  fi
}


function buildup_local_dev_mounts() {
  yq -i '.spec.template.spec.volumes[0].name = "local-dev-volume"' $PROJECT_ROOT/deployment/{{SERVICE_NAME}}_deployment.yml
  yq -i '.spec.template.spec.volumes[0].hostPath.path = strenv(HOST_PATH)' $PROJECT_ROOT/deployment/{{SERVICE_NAME}}_deployment.yml
  yq -i '.spec.template.spec.containers[0].volumeMounts[0].mountPath = "/app/{{SERVICE_NAME}}"' $PROJECT_ROOT/deployment/{{SERVICE_NAME}}_deployment.yml
  yq -i '.spec.template.spec.containers[0].volumeMounts[0].name = "local-dev-volume"' $PROJECT_ROOT/deployment/{{SERVICE_NAME}}_deployment.yml
}


function teardown_local_dev_mounts() {
  yq -i 'del(.spec.template.spec.volumes)' $PROJECT_ROOT/deployment/{{SERVICE_NAME}}_deployment.yml
  yq -i 'del(.spec.template.spec.containers[0].volumeMounts)' $PROJECT_ROOT/deployment/{{SERVICE_NAME}}_deployment.yml
}


# Main Logic

cd $PROJECT_ROOT 2>&1 > /dev/null

if [[ $ACTION = 'create' ]]
then
  info_logger "Initializing The {{SERVICE_SUITE_NAME}} {{SERVICE_NAME}} service..."
  if [[ $LOCAL_DEV ]]
  then
    # Check if Vault container has a secret for the  {{SERVICE_NAME}} service API database. If not, do the needful.
    export CONTAINER_NETWORK_NAME=$(docker inspect -f '{{range $k, $v := .NetworkSettings.Networks}}{{print $k}}{{end}}' $SECRETS_CONTAINER_NAME)
    export DOCKER_INSPECT_FILTER="{{.NetworkSettings.Networks."$CONTAINER_NETWORK_NAME".Gateway}}"
    export VAULT_ADDR="http://$(docker inspect -f $DOCKER_INSPECT_FILTER $SECRETS_CONTAINER_NAME):8200"
    yq -i '.data.secrets_host = strenv(VAULT_ADDR)' $PROJECT_ROOT/deployment/{{SERVICE_NAME}}_configmap.yml
    yq -i '.data.secrets_token = strenv(VAULT_TOKEN)' $PROJECT_ROOT/deployment/{{SERVICE_NAME}}_configmap.yml

    vault kv get secret/$SECRETS_KEY

    if [[ $? != 0 ]]
    then
      vault kv put secret/$SECRETS_KEY secret=$TFD_{{SERVICE_NAME}}_API_DB_PASSWORD
    fi

    # Fetch IP address of the database and secrets
    # containers and inject into ConfigMap
    export DATABASE_ADDR="http://$(docker inspect -f $DOCKER_INSPECT_FILTER $DATABASE_CONTAINER_NAME):5432"
    yq -i '.data.database_host = strenv(DATABASE_ADDR)' $PROJECT_ROOT/deployment/{{SERVICE_NAME}}_configmap.yml
    yq -i '.data.database_user = strenv(DATABASE_USER)' $PROJECT_ROOT/deployment/{{SERVICE_NAME}}_configmap.yml
  else
    yq -i '.data.database_host = strenv(DATABASE_HOST)' $PROJECT_ROOT/deployment/{{SERVICE_NAME}}_configmap.yml
    yq -i '.data.database_user = strenv(DATABASE_USER)' $PROJECT_ROOT/deployment/{{SERVICE_NAME}}_configmap.yml
    yq -i '.data.secrets_host = strenv(SECRETS_HOST)' $PROJECT_ROOT/deployment/{{SERVICE_NAME}}_configmap.yml
    yq -i '.data.secrets_token = strenv(SECRETS_TOKEN)' $PROJECT_ROOT/deployment/{{SERVICE_NAME}}_configmap.yml
  fi

  if [[ $LOCAL_DEV ]]
  then
    export KUBECTL_CONTEXT=minikube
    export IMAGE=schism/{{SERVICE_NAME}}:latest

    export HOST_PATH=/minikube-host
    buildup_local_dev_mounts
  else
    export KUBECTL_CONTEXT=do-nyc1-tfd
    export IMAGE=registry.digitalocean.com/lootdb/{{SERVICE_NAME}}:latest
    doctl registry login
    teardown_local_dev_mounts
  fi

  kubectl config use-context $KUBECTL_CONTEXT
  yq -i '.spec.template.spec.containers[0].image = strenv(IMAGE)' $PROJECT_ROOT/deployment/{{SERVICE_NAME}}_deployment.yml
  kubectl create -f $PROJECT_ROOT/deployment

  # Always revert back to the public Docker Hub image
  # and remove any local dev volumes from manifest
  yq -i '.spec.template.spec.containers[0].image = "schism/{{SERVICE_NAME}}:latest"' $PROJECT_ROOT/deployment/{{SERVICE_NAME}}_deployment.yml
  teardown_local_dev_mounts

  # Reset Config
  yq -i '.data.database_host = ""' $PROJECT_ROOT/deployment/{{SERVICE_NAME}}_configmap.yml
  yq -i '.data.database_user = ""' $PROJECT_ROOT/deployment/{{SERVICE_NAME}}_configmap.yml
  yq -i '.data.secrets_host = ""' $PROJECT_ROOT/deployment/{{SERVICE_NAME}}_configmap.yml
  yq -i '.data.secrets_token = ""' $PROJECT_ROOT/deployment/{{SERVICE_NAME}}_configmap.yml

  info_logger "{{SERVICE_NAME}} service initialized."
  info_logger "Checking for {{SERVICE_NAME}} service availability..."

  get_service_url

  while [[ $SERVICE_URL == "http://null:5000" ]]
  do
    sleep 5
    get_service_url
    echo "..."
  done

  confirm_logger "...{{SERVICE_NAME}} service available: $SERVICE_URL"
  
  info_logger "Running curl test for $SERVICE_URL/ping..."
  ret="1"

  while [[ $ret != 0 ]]
  do
    curl -X GET "$SERVICE_URL/ping" > /dev/null 2>&1
    export ret=$?
    echo '...'
    sleep 5
  done

  confirm_logger "Curl test passed for {{SERVICE_NAME}} service."

elif [[ $ACTION = 'delete' ]]
then
  info_logger "Deleting the {{SERVICE_NAME}} service..."
  if [[ $LOCAL_DEV ]]; then
    # TODO: Delete DB key from Vault instance
    export KUBECTL_CONTEXT=minikube
  else
    export KUBECTL_CONTEXT=do-nyc1-tfd
  fi

  kubectl delete -f $PROJECT_ROOT/deployment
  confirm_logger "...{{SERVICE_NAME}} service deleted."
  confirm_logger "...the full suite of The {{SERVICE_SUITE_NAME}} services have been deleted."
else
  error_logger "Please select a valid action."
  usage
fi

cd - 2>&1 > /dev/null