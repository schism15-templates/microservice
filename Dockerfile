# syntax=docker/dockerfile:1
FROM ubuntu:22.04

ARG CURRENT_UID=1000

RUN apt update -y
RUN apt upgrade -y
RUN apt install -y g++ gcc python3 python3-venv python3-dev \
    libc6 linux-allwinner-5.17-headers-5.17.0-1010 \
    libjansson-dev libxml2-dev libicu70

RUN cd /lib/x86_64-linux-gnu && ln -s libpcre.so.3.13.3 libpcre.so.1 \
    && ln -s libicui18n.so.70.1 libicui18n.so.72 \
    && ln -s libicuuc.so.70.1 libicuuc.so.72 \
    && ln -s libicudata.so.70.1 libicudata.so.72 \
    && ln -s libcrypt.so.1.1.0 libcrypt.so.2 \
    && cd -

RUN addgroup --gid $CURRENT_UID appgroup
RUN adduser \
    --uid $CURRENT_UID \
    --disabled-password \
    --home /app \
    --gecos "" \
    --ingroup appgroup \
    appuser

USER appuser

WORKDIR /app
ENV VIRTUAL_ENV=/app/.venv
RUN /usr/bin/python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN pip install --upgrade pip wheel
COPY requirements/requirements.txt requirements/requirements.txt
COPY requirements/dev-requirements.txt requirements/dev-requirements.txt
RUN pip install -r requirements/requirements.txt -r requirements/dev-requirements.txt
EXPOSE 5000
COPY . .
ENV FLASK_APP={{SERVICE_NAME}}.wsgi:app
# ENV FLASK_ENV="${{SERVICE_NAME}}_API_ENV"
CMD ["./deployment/run.sh"]
