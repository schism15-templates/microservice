#!/bin/sh

uwsgi --socket 0.0.0.0:5000 --protocol=http --py-autoreload=1 -w {{SERVICE_NAME}}.wsgi:app