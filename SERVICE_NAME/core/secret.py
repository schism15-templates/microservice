import os

import hvac

# TODO: Load Secrets config here until I work out
# config reloading
def get_secret(path: str) -> dict:
    url = os.environ.get("FLASK_{{SERVICE_NAME}}__SECRETS__HOST")
    token = os.environ.get("FLASK_{{SERVICE_NAME}}__SECRETS__TOKEN")
    client = hvac.Client(url=url, token=token)
    return client.secrets.kv.read_secret_version(path=path)
