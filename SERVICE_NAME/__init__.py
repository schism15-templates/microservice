import os

from flask import (
    Flask,
    make_response,
)
from flask_sqlalchemy import SQLAlchemy

from .core import get_secret

db = SQLAlchemy()


def create_app(test_config=None):
    # Create the app and load
    # config
    app = Flask(__name__)

    if test_config:
        app.config.from_mapping(test_config)
    else:
        app.config.from_prefixed_env()

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # Initialize the app with the
    # Flask-SQLalchemy extension
    db_host = app.config[{{SERVICE_NAME}}]["DATABASE"]["HOST"].partition("://")[
        2
    ]  # TODO: Use regex
    db_user = app.config[{{SERVICE_NAME}}]["DATABASE"]["USER"]
    db_pass = get_secret("{{SERVICE_NAME}}-api-db")["data"]["data"]["secret"]
    app.config[
        "SQLALCHEMY_DATABASE_URI"
    ] = f"postgresql+psycopg2://{db_user}:{db_pass}@{db_host}/{{SERVICE_NAME}}"
    sqla_tracking = app.config[{{SERVICE_NAME}}]["DATABASE"]["SQLA_TRACKING"]
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = sqla_tracking

    db.init_app(app)

    # Configure routes
    @app.route("/ping")
    def pong():
        return "pong!"

    return app
